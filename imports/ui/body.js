import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Products } from '../api/products.js';

import './prod.js';
import './body.html';

Template.body.onCreated(function bodyOnCreated() {
  this.dataDict= new ReactiveDict();
  Meteor.subscribe('Products');
});

Template.body.helpers({
  prods() {
    const instance = Template.instance();
    return Products.find({}).fetch();
  },
  isEditable() {
    var t = Template.instance();
    return t.dataDict.get("changeTemplate");
  },
});

Template.body.events({
  'submit #newcard'(event) {
    // Prevent default browser form submit
    event.preventDefault();
    // Get value from form element
    const target = event.target;
    const text = target.text.value;
    const count = target.count.value;
    // Insert a product into the collection
    Meteor.call('prods.insert', text,count);
    // Clear form
    target.text.value = '';
    target.count.value='';
  },
  'click #edit':function(e,t){
    var id= this._id;
    t.dataDict.set("changeTemplate",id);
  },
  'click #delete':function(e,t){
    var prodId= this._id;
    console.log(prodId);
    Meteor.call('prods.delete',prodId);
  },
});
