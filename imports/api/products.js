import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Products = new Mongo.Collection('Products');

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish Products that are public or belong to the current user
  Meteor.publish('Products', function() {
    return Products.find({
    });
  });
}

Meteor.methods({
  'prods.insert'(text,count) {
    check(text, String);
    check(count, String);
    Products.insert({
      name:text,
      number:count,
      createdAt: new Date(),

    });
  },
  'prods.delete'(prodId) {
    check(prodId, String);
    const prod = Products.findOne(prodId);
    Products.remove(prodId);
  },
  'prods.update'(prodId,text,count) {
    check(prodId, String);
    check(text, String);
    check(count, String);
    const prod = Products.findOne(prodId);
    Products.update({prodId},{$set:{name:text,number:count,modifiedAt:new Date()}});
  },



});
